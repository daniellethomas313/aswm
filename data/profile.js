function loadProfile() {
  "name": "Danielle Thomas",
  "major": "Interactive Arts & Media", // Name and Major are both 'strings', bake sure all your letters stay in the quotes
  "location": 60605, // Enter your zipcode.. should only be numbers
  // The courses property is an Array
  "courses": [
    {
      "title": "Advanced Scripting for Web and Mobile",
      "time": '2016-02-01 12:30:00' // for HH, use 24 hour clock, for SS, 00 will do
    },
    {
      "title": 'Directed Study',
      "time": '2016-02-04 12:15:00'
    } // if you put a comma after this bracket, you can add more courses using the syntax above.
      // the very last bracket should NOT have a comma after it.
  ]
}
