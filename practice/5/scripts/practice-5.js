function Request(url, successCallback, failCallback) {
	// Declare our default variables
	var XHR;
	var defaultCallback = function(evt) {
		console.warn(evt.type + 'event fired:');
	};

	XHR = new XMLHttpRequest();
	XHR.open('GET', url);
	XHR.overrideMimeType('text/xml');
	XHR.addEventListener('load', successCallback || defaultCallback);
	XHR.addEventListener('error', failCallback || defaultCallback);
	XHR.send();
}

Request('list.html', function(evt) {
	//get all the li elements in the response and store it in an array
	var list = this.responseXML.querySelectorAll('li');
	var ul = document.querySelector('main > ul[data-replace]');
	[].forEach.call(list, function(li){
		console.log(li);
		var new_li = document.createElement('li');
		new_li.textContent = li.textContent;
		ul.appendChild(new_li);
	});
	console.log(lis, ul);
});

/*Wrap this in a window load function*/
//Grab another file from different directory
Request('/index.html', function(evt){
	console.log(this.responseXML.querySelector('h1').textContent);
});