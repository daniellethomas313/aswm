window.addEventListener('load', function() {
	var clock_in = document.getElementById('clockIn');
    var clock_out = document.getElementById('clockOut');
    var greeting = document.getElementById('greeting');
    var date_in, date_out;

	clock_in.addEventListener('click', function(e){ 
		console.log("You have clocked in");
		clockIn();
	});
    clock_out.addEventListener('click', function(e){ 
        console.log("You have clocked out");
        clockOut();
    });
});

function clockIn(evt) {
	date_in = new Date();
	greeting.textContent = date_in;
}

function clockOut(evt) {
    date_out = new Date();
    greeting.textContent = date_out;
    timeWorked();
}

function timeWorked() {
    var timeWorked = date_out - date_in;
    timeWorked /= 60000;
    greeting.textContent = "You worked " + timeWorked.toPrecision(4) + " minutes";

}

function myFunction(arr) {
    var out = "";
    var i;
    for(i = 0; i < arr.length; i++) {
        out += '<a href="' + arr[i].url + '">' + arr[i].display + '</a><br>';
    }
    document.getElementById("data").innerHTML = out;
}

myFunction(myArray);

var xmlhttp = new XMLHttpRequest();
var url = "../data/profile.js";

xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        var myArr = JSON.parse(xmlhttp.responseText);
        myFunction(myArr);
    }
};
xmlhttp.open("GET", url, true);
xmlhttp.send();

function myFunction(arr) {
    var out = "";
    var i;
    for(i = 0; i < arr.length; i++) {
        out += '<a href="' + arr[i].url + '">' + 
        arr[i].display + '</a><br>';
    }
    document.getElementById("data").innerHTML = out;
}

