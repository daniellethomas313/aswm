Declare a function that takes only one argument.
If more than one argument is given, throw an error.
Declare a function that takes unlimited arguments.
The function declaration should have no declared arguments
Use the arguments object in the function body to do something of your choosing
return something of your choosing.
Declare a function that returns another function
The function should take some data input, perhaps an array or an object
The function should return a function capable of acting on that input. (this is closure)
Declare a function that takes a function as an argument
The function should take at least 2 arguments
One of the arguments should be data
The function argument should operate on the data.
return the modified data
Note while this one may seem absurd, it's a very common pattern that I'll outline in an example next week.
Declare a function that takes a DOMElement as an argument
create a new element and assign it to a variable
insert the new element before to the old
remove the original element
return the newly created element
There is an easier way to do this, but I'm interested in this, the hard way!
