function loadDoc(url, cfunc) {
  var xhttp;
  xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      cfunc(xhttp);
    }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}

function myFunction(xhttp) {
 	var data = JSON.parse(xhttp.responseText);
	var neighborhood = data.results[0].address_components[2].long_name;
	var neighborhood_formatted = parseLocation(neighborhood, "-");

	document.getElementById("demo").innerHTML = data.results[0].formatted_address + "<p>Neighborhood: " + neighborhood + "</p>";
  	console.log(neighborhood);

  
	getNeighborhoodInfo("http://www.choosechicago.com/neighborhoods-and-communities/intro/"+neighborhood_formatted+"/",cNeighborhood);
  	//console.log(neighborhood_formatted);
}

function getNeighborhoodInfo(url, cfunc) {
	var xhttp;
  	xhttp = new XMLHttpRequest();
  	/*xhttp.onreadystatechange = function() {
	    if (xhttp.readyState == 4 && xhttp.status == 200) {
	      	cfunc(http);
	    }
	};*/
  	xhttp.open("GET", url);
  	xhttp.addEventListener('load', cfunc);
	xhttp.send();
}

function cNeighborhood(xhttp) {
	var data = JSON.parse(xhttp.responseText);

	document.getElementById("neighborhood-info").innerHTML = data;
}

function parseLocation(location, imploder) {
	//searches the entire string for any instance of a blank space
	if (location.search(" ") != -1) {
		/*perform a global replacement, because the function only replaces the first occurrence, and then returns the result as a new string, it doesn't not modify the original string, because strings are immutable*/
		var parsedLocation = location.replace(/ /g,imploder);
	}
	return parsedLocation;
}

window.addEventListener('load', function() {
	document.querySelector('form').addEventListener('submit', function(evt){
		evt.preventDefault();
		var target = evt.target;
		var city = target["city"].value;
		var state = target["state"].value;
		var address = target["address"].value;

		console.log(parseLocation(address,"+"));

		loadDoc("http://maps.google.com/maps/api/geocode/json?address="+address+","+city+","+state+"&sensor=false", myFunction);
	});
});






